const checkValue = card => {
	switch (card.value) {
		case 'QUEEN':
			card.value = 10;
			break;
		case 'KING':
			card.value = 10;
			break;
		case 'JACK':
			card.value = 10;
			break;
		case 'ACE':
			card.value = 11;
			break;
		default:
			break;
	}
	return card;
};

export default checkValue;
