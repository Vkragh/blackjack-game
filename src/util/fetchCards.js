const fetchCards = async (deckId, amount) => {
	const res = await fetch(
		`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=${amount}`
	);
	const data = await res.json();

	return data;
};

export default fetchCards;
