const fetchDeck = async () => {
	const res = await fetch(
		'https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1'
	);
	const data = await res.json();

	return data;
};

export default fetchDeck;
