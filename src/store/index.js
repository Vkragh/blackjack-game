import Vue from 'vue';
import Vuex from 'vuex';
import fetchDeck from '@/util/fetchDeck';
import fetchCards from '@/util/fetchCards';
import checkValue from '@/util/checkCardValue';

Vue.use(Vuex);
export default new Vuex.Store({
	state: {
		gameActive: false,
		dealerActive: false,
		deckId: '',
		amountToDraw: 1,
		dealer: {
			cards: [],
		},
		player: {
			name: '',
			cards: [],
		},
	},
	mutations: {
		setGameActive: (state, payload) => {
			state.gameActive = payload;
		},
		setDeckId: (state, payload) => {
			state.deckId = payload;
		},
		setAmountToDraw: (state, payload) => {
			state.amountToDraw = payload;
		},
		setDealerCards: (state, payload) => {
			state.dealer.cards.push(payload);
		},
		setDealerActive: (state, payload) => {
			state.dealerActive = payload;
		},
		setPlayerCards: (state, payload) => {
			state.player.cards.push(payload);
		},
		setPlayerName: (state, payload) => {
			state.player.name = payload;
		},
		clearDecks: state => {
			state.player.cards = [];
			state.dealer.cards = [];
		},
	},
	getters: {
		getPlayerScore: state => {
			return state.player.cards.reduce((acc, am) => {
				return (acc += Number(am.value));
			}, 0);
		},
		getDealerScore: state => {
			return state.dealer.cards.reduce((acc, am) => {
				return (acc += Number(am.value));
			}, 0);
		},
	},
	actions: {
		async fetchNewDeck({ commit }) {
			const data = await fetchDeck();
			commit('setDeckId', data.deck_id);
		},
		async fetchCards({ commit, state }, user) {
			const data = await fetchCards(state.deckId, state.amountToDraw);
			if (user === 'dealer') {
				data.cards.forEach(card => {
					commit('setDealerCards', checkValue(card));
				});
			} else {
				data.cards.forEach(card => {
					commit('setPlayerCards', checkValue(card));
				});
			}
		},
	},
});
